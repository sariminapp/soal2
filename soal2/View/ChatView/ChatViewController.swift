//
//  ChatViewController.swift
//  soal2
//
//  Created by Oscar Perdanakusuma Adipati on 28/12/20.
//

import Foundation
import UIKit

class ChatViewController: BaseParentController {
    @IBOutlet weak var chatStatusView: UIView!
    @IBOutlet weak var cahtDateView: UIView!
    @IBOutlet weak var lblChatStatus: UILabel!
    @IBOutlet weak var lblChatDate: UILabel!
    @IBOutlet weak var lblChatPrivacy: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var dateChat: String?
    var doctorName: String?
    var chatStatus: String?
    var incomingOutcomingArray = [1,2,2,1,2,1]
    var chatArray = ["wow, why so easy?", "Because you learn a lot from your doctor", "You must become a good doctor", "how far you can go?", "Not that far", "i always want to fly higher and higher"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.navigationController == nil {
            return
        }
        setEnableTapGestureOnMainView(isEnable: false)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.title = doctorName
        setRoundedView(view: lblChatPrivacy, bgColor: Constant.sharedIns.color_green_500)
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if chatStatus != "" {
            lblChatStatus.text = chatStatus
            chatStatusView.isHidden = false
        } else {
            chatStatusView.isHidden = true
        }
        lblChatDate.text = dateChat
        tableView.reloadData()
        DispatchQueue.main.async {
            self.tabBarController?.tabBar.isHidden = true
        }
    }
            
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
}

extension ChatViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return incomingOutcomingArray.count //can change with count array which append from API object
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "chatCell", for: indexPath) as! ChatCellView
        let row = indexPath.row
        cell.selectionStyle = .none
        if (incomingOutcomingArray[row] == 1) {
            cell.lblName.textColor = UIColor(string: Constant.sharedIns.color_grey_300)
            cell.lblName.text = "\(CustomUserDefaults.shared.getUsername())"
            cell.lblName.textAlignment = .right
            cell.incomingView.isHidden = true
            cell.outgoingView.isHidden = false
            cell.lblChat.text = chatArray[row]
            cell.lblChat.textColor = .white
            cell.chatView.backgroundColor = .green
            cell.chatView.roundCorners(corners: [.topLeft, .bottomLeft, .bottomRight], radius: 20)
        } else if (incomingOutcomingArray[row] == 2) {
            cell.lblName.textColor = UIColor(string: Constant.sharedIns.color_grey_300)
            cell.lblName.text = doctorName
            cell.lblName.textAlignment = .left
            cell.incomingView.isHidden = false
            cell.outgoingView.isHidden = true
            cell.lblChat.text = chatArray[row]
            cell.lblChat.textColor = .black
            cell.chatView.backgroundColor = .lightGray
            cell.chatView.roundCorners(corners: [.topRight, .bottomLeft, .bottomRight], radius: 20)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        //if selected
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 177
    }
    
}
