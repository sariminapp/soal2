//
//  CodeVerificationViewController.swift
//  soal2
//
//  Created by Oscar Perdanakusuma Adipati on 27/12/20.
//

import Foundation
import UIKit
import PinCodeTextField
import KWVerificationCodeView
import UserNotifications

class CodeVerificationViewController: InitialParentController, UNUserNotificationCenterDelegate {
    @IBOutlet weak var txtCodeVerification: KWVerificationCodeView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblErrorTime: UILabel!
    @IBOutlet weak var lblChangePhoneNumber: UILabel!
    @IBOutlet weak var btnConfirmation: UIButton!
    var countryCode: String?
    var phoneNumber: String?
    var resultMessage: String?
    var timer: Timer?
    var counter = 30
    let userNotificationCenter = UNUserNotificationCenter.current()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customNavBar.isHidden = true
        setEnableTapGestureOnMainView(isEnable: false)
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3)) {
            self.txtCodeVerification.becomeFirstResponder()
        }
        
        txtCodeVerification.delegate = self
        txtCodeVerification.keyboardType = .numberPad
        
//        if #available(iOS 12.0, *) {
//            self.txtCodeVerification.textContentType = .oneTimeCode
//        }
        
        setRoundedButton(button: btnConfirmation, textColor: Constant.sharedIns.color_white, bgColor: Constant.sharedIns.color_green_800, isBold: true)
        
        lblTitle.attributedText =
            NSMutableAttributedString()
                .bold("Masukan 6 kode ")
                .normal("yang telah diterima via SMS ke nomor yang telah anda masukkan")
        
        addTapEventForView(view: lblChangePhoneNumber, action: #selector(changePhoneClicked))
        addTapEventForView(view: btnConfirmation, action: #selector(confirmationClicked))
        /// for example without SMS but use notification, if use SMS not use this
        self.userNotificationCenter.delegate = self
        self.requestNotificationAuthorization()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateCounter), userInfo: nil, repeats: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.sendNotification()
    }
            
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        timer?.invalidate()
    }
    
    @objc func updateCounter() {
        //example functionality
        if counter == 1 {
            lblErrorTime.text = ""
        } else if counter != 0 {
            lblErrorTime.text = "\(counter) detik"
            print("\(counter) detik")
            counter -= 1
        } else {
            if let timer = self.timer {
                timer.invalidate()
                self.timer = nil
            }
        }
    }
    
    @objc func changePhoneClicked () {
        popViewController()
    }
    
    @objc func confirmationClicked () {
        ///if use API
//        VerifyAPI.validateVerificationCode(self.countryCode!, self.phoneNumber!, txtCodeVerification.getVerificationCode()) { checked in
//            if (checked.success) {
//                let storyboard = UIStoryboard(name: Constant.sharedIns.MAIN_STORYBOARD, bundle: nil)
//                let navCon = storyboard.instantiateViewController(withIdentifier: Constant.sharedIns.PROFILE_NAV) as! UINavigationController
//                let vc = navCon.viewControllers.first as! ProfileViewController
//                vc.modalPresentationStyle = .fullScreen
//                self.navigationController?.pushViewController(vc, animated: true)
//            } else {
//                AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: Wordings.msg_input_otp_incorrect, actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
//                return
//            }
//        }
        ///if not use API
        if (txtCodeVerification.getVerificationCode() == "123456") {
            let storyboard = UIStoryboard(name: Constant.sharedIns.MAIN_STORYBOARD, bundle: nil)
            let navCon = storyboard.instantiateViewController(withIdentifier: Constant.sharedIns.PROFILE_NAV) as! UINavigationController
            let vc = navCon.viewControllers.first as! ProfileViewController
            vc.modalPresentationStyle = .fullScreen
            navigationController?.pushViewController(vc, animated: true)
        } else {
            AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: Wordings.msg_input_otp_incorrect, actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
            return
        }
    }
    
    func requestNotificationAuthorization() {
        let authOptions = UNAuthorizationOptions.init(arrayLiteral: .alert, .badge, .sound)
        self.userNotificationCenter.requestAuthorization(options: authOptions) { (success, error) in
            if let error = error {
                print("Error: ", error)
            }
        }
    }

    func sendNotification() {
        let notificationContent = UNMutableNotificationContent()
        notificationContent.title = "OTP code"
        notificationContent.body = "123456"
        notificationContent.badge = NSNumber(value: 1)
        if let url = Bundle.main.url(forResource: "ic_profile",
                                        withExtension: "png") {
            if let attachment = try? UNNotificationAttachment(identifier: "ic_profile",
                                                                url: url,
                                                                options: nil) {
                notificationContent.attachments = [attachment]
            }
        }
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5,
                                                        repeats: false)
        let request = UNNotificationRequest(identifier: "testNotification",
                                            content: notificationContent,
                                            trigger: trigger)
        userNotificationCenter.add(request) { (error) in
            if let error = error {
                print("Notification Error: ", error)
            }
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        completionHandler()
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .badge, .sound])
    }
}

extension CodeVerificationViewController: KWVerificationCodeViewDelegate {
    func didChangeVerificationCode() {
        ///
    }
}
