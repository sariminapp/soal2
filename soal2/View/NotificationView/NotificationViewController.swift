//
//  NotificationViewController.swift
//  soal2
//
//  Created by Oscar Perdanakusuma Adipati on 28/12/20.
//

import Foundation
import UIKit

class NotificationViewController: BaseParentController {
    @IBOutlet weak var notificationView: UIView!
    @IBOutlet weak var tabViewIndicator: UIView!
    @IBOutlet weak var segmentedFrameView: UIView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    var statusChatArray = ["Chat Selesai"]
    var doctorNameChatArray = ["dr. Nadia A. Wulansari, S.PD, KHOM"]
    var doctorServiceChatArray = ["Spesialis Penyakit Dalam"]
    var dateChatArray = ["5 Januari 2020"]
    var statusJanjiArray = [""]
    var doctorNameJanjiArray = ["dr. Untung Selalu, S.PD, KHOM"]
    var doctorServiceJanjiArray = ["Spesialis Bedah Umum"]
    var dateJanjiArray = ["15 Juni 2022"]
    var docNameArray = [String]()
    var docServiceArray = [String]()
    var dateArray = [String]()
    var chatStatusArray = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.navigationController == nil {
            return
        }
        setEnableTapGestureOnMainView(isEnable: false)
        setupSegmentedContol()
        tableView.delegate = self
        tableView.dataSource = self
        setShadowForView(view: segmentedFrameView)
        self.view.bringSubviewToFront(segmentedFrameView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setBackgroundViewByThemeColor(view: segmentedFrameView)
        setNavBarThemeColor()
        setSegmentedPosition(index: CustomUserDefaults.shared.getHeaderOpenCase())
        if (CustomUserDefaults.shared.getHeaderOpenCase() == 0) {
            segmentedControl.selectedSegmentIndex = 0
            fetchChat()
        } else if (CustomUserDefaults.shared.getHeaderOpenCase() == 1) {
            segmentedControl.selectedSegmentIndex = 1
            fetchJanji()
        }
        DispatchQueue.main.async {
            self.tabBarController?.tabBar.isHidden = true
            self.notificationView.roundCorners(corners: [.topLeft, .topRight], radius: 20)
        }
    }
            
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }
    
    func setShadowForView(view: UIView){
        let layer = view.layer
        layer.shadowOffset = CGSize(width: 0, height: 1)
        layer.shadowOpacity = 0.15
        layer.shadowRadius = 2.0
        layer.shadowColor = UIColor.gray.cgColor
        layer.masksToBounds = false
    }
    
    private func imageWithColor(color: UIColor) -> UIImage {
        let rect = CGRect(x: 0.0, y: 0.0, width:  1.0, height: 1.0)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(color.cgColor)
        context!.fill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupSegmentedContol() {
        setBackgroundViewByThemeColor(view: segmentedFrameView)
        
        let segAttributesNormal: NSDictionary = [
            NSAttributedString.Key.foregroundColor: UIColor(string: Constant.sharedIns.color_grey_300),
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.semibold)
        ]
        
        segmentedControl.setTitleTextAttributes(segAttributesNormal as? [NSAttributedString.Key : Any], for: UIControl.State.normal)
        
        let segAttributesSelected: NSDictionary = [
            NSAttributedString.Key.foregroundColor: UIColor(string: Constant.sharedIns.color_grey_500),
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.semibold)
        ]
        
        segmentedControl.setTitleTextAttributes(segAttributesSelected as? [NSAttributedString.Key : Any], for: UIControl.State.selected)
        
        segmentedControl.setBackgroundImage(imageWithColor(color: UIColor.clear), for: .normal, barMetrics: .default)
        
        segmentedControl.setBackgroundImage(imageWithColor(color: UIColor(string: Constant.sharedIns.color_grey_300_alpha)), for: .highlighted, barMetrics: .default)
        
        segmentedControl.setBackgroundImage(imageWithColor(color: UIColor.clear), for: .selected, barMetrics: .default)
        
        segmentedControl.setDividerImage(imageWithColor(color: UIColor.clear), forLeftSegmentState: .normal, rightSegmentState: .normal, barMetrics: .default)
    }
    
    func setSegmentedPosition(index: Int) {
        
        /// Animate tab indicator
        if(index > 0){
            let xPos =  self.view.frame.width * (0.5 * CGFloat(index))
            
            UIView.animate(withDuration: 0.2, animations: {
                let translation = CGAffineTransform(translationX: xPos, y: 0)
                self.tabViewIndicator.transform = translation
            })
        }else{
            UIView.animate(withDuration: 0.2, animations: {
                self.tabViewIndicator.transform = CGAffineTransform.identity
            })
        }
        
        segmentedControl.selectedSegmentIndex = index
    }
    
    @IBAction func selectedTab(_ sender: UISegmentedControl) {
        print("SEGMENTED_INDE X: \(sender.selectedSegmentIndex)")
        
        let index = sender.selectedSegmentIndex
        CustomUserDefaults.shared.setHeaderOpenCase(index)
        setSegmentedPosition(index: index)
        switch(segmentedControl.selectedSegmentIndex)
        {
        case 0:
            fetchChat()
            break
        case 1:
            fetchJanji()
            break
            
        default:
            fetchChat()
            break
        }
    }
    
    func fetchChat() {
        removeAll()
        docNameArray = doctorNameChatArray
        docServiceArray = doctorServiceChatArray
        dateArray = dateChatArray
        chatStatusArray = statusChatArray
        tableView.reloadData()
    }
    
    func fetchJanji() {
        removeAll()
        docNameArray = doctorNameJanjiArray
        docServiceArray = doctorServiceJanjiArray
        dateArray = dateJanjiArray
        chatStatusArray = statusJanjiArray
        tableView.reloadData()
    }
    
    func removeAll() {
        docNameArray.removeAll()
        docServiceArray.removeAll()
        dateArray.removeAll()
        chatStatusArray.removeAll()
    }
}

extension NotificationViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return docNameArray.count //can change with count array which append from API object
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "notificationCell", for: indexPath) as! NotificationCellView
        let row = indexPath.row
        cell.selectionStyle = .none
        let dotSize = 20
        let firstDotView = UIView()
        firstDotView.frame = CGRect(x: 0, y: 0, width: dotSize, height: dotSize)
        firstDotView.backgroundColor = .green
        firstDotView.layer.cornerRadius = 10
        firstDotView.layer.zPosition = 1
        if (CustomUserDefaults.shared.getHeaderOpenCase() == 0) {
            cell.lblDoctorName.textColor = UIColor(string: Constant.sharedIns.color_green_800)
            cell.lblDate.text = dateArray[row]
            cell.lblDoctorName.text = docNameArray[row]
            cell.lblDoctorSpecialist.text = docServiceArray[row]
            cell.lblStatus.text = chatStatusArray[row]
            cell.lblStatus.isHidden = false
            //you can do not show this last line if you add detection isRead
            cell.imgDoctor.addSubview(firstDotView)
        } else if (CustomUserDefaults.shared.getHeaderOpenCase() == 1) {
            cell.lblDoctorName.textColor = UIColor(string: Constant.sharedIns.color_green_800)
            cell.lblDate.text = dateArray[row]
            cell.lblDoctorName.text = docNameArray[row]
            cell.lblDoctorSpecialist.text = docServiceArray[row]
            cell.lblStatus.text = chatStatusArray[row]
            cell.lblStatus.isHidden = true
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        let storyboard = UIStoryboard(name: Constant.sharedIns.NOTIFICATION_STORYBOARD, bundle: nil)
        let navCon = storyboard.instantiateViewController(withIdentifier: Constant.sharedIns.CHAT_NAV) as! UINavigationController
        let vc = navCon.viewControllers.first as! ChatViewController
        let row = indexPath.row
        vc.tabBarController?.tabBar.isHidden = true
        vc.chatStatus = chatStatusArray[row]
        vc.dateChat = dateArray[row]
        vc.doctorName = docNameArray[row]
        vc.modalPresentationStyle = .fullScreen
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 177
    }
    
}


