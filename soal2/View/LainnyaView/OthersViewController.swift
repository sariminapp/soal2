//
//  OthersViewController.swift
//  soal2
//
//  Created by Oscar Perdanakusuma Adipati on 28/12/20.
//

import Foundation
import UIKit

class OthersViewController: BaseParentController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.navigationController == nil {
            return
        }
        setEnableTapGestureOnMainView(isEnable: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.async {
            self.tabBarController?.tabBar.isHidden = false
        }
    }
            
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
}

