//
//  BaseParentController.swift
//  homecredit
//
//  Created by Oscar Perdanakusuma - Private on 18/12/17.
//  Copyright © 2017 Home Credit Indonesia. All rights reserved.
//

import UIKit

class BaseParentController : PINLockParentController {
    enum titleType{
        case profile
        case title
        case customTitle
    }
    enum type{
        case flexifast
        case myDreamIntro
        case myDreamProgress
        case delete
        case settings
        case contactUs
        case none
        case close
        case logout
        case addContact
        case addSchedule
        case edit
        case done
    }
    
    var rightBarButton : UIBarButtonItem?
    var currButtonType: type = .none
    var lblOtherResult = UILabel()
    var lblOthersResult = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Auto set right bar button
        setRightBarButtonOnNavigation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        isShouldCancelAPI = true
        addObserverNotification()
        //animateBlinking(type: currButtonType)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        removeObserverNotification()
        if(isShouldCancelAPI){
//            cancelAPISession()
        }
    }
    
    func addObserverNotification(){
        ///Always Remove
        removeObserverNotification()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.openFlexiFastPage(_:)),name: Notification.Name.NOTIF_OPEN_FLEXIFAST, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.openMyDreamIntro(_:)),name: Notification.Name.NOTIF_OPEN_MY_DREAM, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.openContractDetail(_:)),name: Notification.Name.NOTIF_OPEN_CONTRACT, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.openInstallmentSchedule(_:)),name: Notification.Name.NOTIF_OPEN_INST_SCHEDULE, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.openHowToPay(_:)),name: Notification.Name.NOTIF_OPEN_HOW_TO_PAY, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.openPaymentLocator(_:)),name: Notification.Name.NOTIF_OPEN_PAYMENT_LOC, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.openCreditSim(_:)),name: Notification.Name.NOTIF_OPEN_GENERAL_SIMULATION, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.openPosLocator(_:)),name: Notification.Name.NOTIF_OPEN_POS_LOCATOR, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.openEShops(_:)),name: Notification.Name.NOTIF_OPEN_E_SHOPS, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.openOurGuarantee(_:)),name: Notification.Name.NOTIF_OPEN_OUR_GUARANTEE, object: nil)
        
    }
    
    func removeObserverNotification(){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.NOTIF_OPEN_FLEXIFAST, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.NOTIF_OPEN_MY_DREAM, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.NOTIF_OPEN_CONTRACT, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.NOTIF_OPEN_INST_SCHEDULE, object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name.NOTIF_OPEN_HOW_TO_PAY, object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name.NOTIF_OPEN_PAYMENT_LOC, object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name.NOTIF_OPEN_GENERAL_SIMULATION, object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name.NOTIF_OPEN_POS_LOCATOR, object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name.NOTIF_OPEN_E_SHOPS, object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name.NOTIF_OPEN_OUR_GUARANTEE, object: nil)
    }
    
    func setRightBarButtonOnNavigation(){
        
    }
    
    func setBarButtonItem(type: titleType, titleName: String, imageName: String){
        let navView = UIView()
        let label = UILabel()
        let image = UIImageView()
        
        switch type{
        case .profile:
            label.text = " \(titleName)"
            label.textColor = .white
            label.sizeToFit()
            label.center = navView.center
            label.textAlignment = NSTextAlignment.center
            label.adjustsFontSizeToFitWidth = true

            // Create the image view
            image.image = UIImage(named: "\(imageName)")
            // To maintain the image's aspect ratio:
            let imageAspect = image.image!.size.width/image.image!.size.height
            // Setting the image frame so that it's immediately before the text:
            if (Constant.checkDeviceIsIpad() == true) {
                print("this device is iPad")
                image.frame = CGRect(x: -20, y: 5, width: label.frame.size.height*imageAspect, height: label.frame.size.height)
            } else {
                image.frame = CGRect(x: 0, y: 5, width: label.frame.size.height*imageAspect, height: label.frame.size.height)
            }
            image.contentMode = UIView.ContentMode.scaleAspectFit
            image.image = image.image!.withRenderingMode(.alwaysTemplate)
            image.tintColor = .white

            let customLeftButton = UIButton(frame: CGRect(x: 0, y: 0, width: 130, height: 30))
            customLeftButton.addSubview(image)
            customLeftButton.setTitle("\(titleName)", for: .normal)
            customLeftButton.titleLabel?.font =  .systemFont(ofSize: 15)
            customLeftButton.addTarget(self, action: #selector(profilePage), for: .touchUpInside)
            let leftButton = UIBarButtonItem(customView: customLeftButton)
            self.navigationItem.leftBarButtonItem  = leftButton
            break
        case .title:
            label.text = " \(titleName)"
            label.textColor = .white
            label.sizeToFit()
            label.center = navView.center
            label.textAlignment = NSTextAlignment.center
            
            // Create the image view
            image.image = UIImage(named: "\(imageName)")
            // To maintain the image's aspect ratio:
            let imageAspect = image.image!.size.width/image.image!.size.height
            // Setting the image frame so that it's immediately before the text:
            image.frame = CGRect(x: label.frame.origin.x-label.frame.size.height*imageAspect, y: label.frame.origin.y, width: label.frame.size.height*imageAspect, height: label.frame.size.height)
            image.contentMode = UIView.ContentMode.scaleAspectFit
            if (label.text!.isEmpty == true) {
                image.center = navView.center
            }
            
            // Add both the label and image view to the navView
            navView.addSubview(label)
            navView.addSubview(image)
            
            // Set the navigation bar's navigation item's titleView to the navView
            self.navigationItem.titleView = navView
            
            // Set the navView's frame to fit within the titleView
            navView.sizeToFit()
            break
        case .customTitle:
            label.text = " \(titleName)"
            label.textColor = .white
            label.sizeToFit()
            
            // Create the image view
            image.image = UIImage(named: "\(imageName)")
            // To maintain the image's aspect ratio:
            let imageAspect = image.image!.size.width/image.image!.size.height
            // Setting the image frame so that it's immediately before the text:
            image.frame = CGRect(x: label.frame.origin.x-label.frame.size.height*imageAspect, y: label.frame.origin.y, width: label.frame.size.height*imageAspect*2, height: label.frame.size.height*2)
            image.contentMode = UIView.ContentMode.scaleAspectFit
            image.center = navView.center
            
            // Add both the label and image view to the navView
//            navView.addSubview(label)
            navView.addSubview(image)
            
            // Set the navigation bar's navigation item's titleView to the navView
            self.navigationItem.titleView = navView
            
            // Set the navView's frame to fit within the titleView
            navView.sizeToFit()
            break
        }
    }
    
    func setRightBarButtonItem(type: type){
        var icMenu : UIImage?
        var selector : Selector?
        
        switch type{
        case .flexifast:
            icMenu = UIImage(named: "ic_coin_flexifast_white")
            selector = #selector(openFlexiFastPage(_:))
            break
        case .myDreamIntro:
            icMenu = UIImage()
            selector =  #selector(openMyDreamIntro(_:))
            break
        case .myDreamProgress:
            icMenu = UIImage()
            selector =  #selector(openMyDreamProgress(_:))
            break
        case .contactUs:
            icMenu = UIImage(named: "ic_contact_us")
            selector = #selector(openContactUsPage(_:))
            break
        case.delete:
            icMenu = UIImage(named: "ic_delete_white")
            selector = #selector(deleteMessage(_:))
            break
        case .settings:
            icMenu = UIImage(named: "ic_settings")
            selector = #selector(openSettingsPage(_:))
            break
        case .none:
            icMenu = UIImage()
            selector = nil
            break
        case .close:
            icMenu = UIImage(named: "ic_close")
            selector = #selector(closePage)
            break
        case .logout:
            icMenu = UIImage(named: "ic_logout")
            selector = #selector(logout)
            break
        case .addContact:
            icMenu = UIImage(named: "ic_plus")
            selector = #selector(addContact)
            break
        case .addSchedule:
            icMenu = UIImage(named: "ic_plus")
            selector = #selector(addSchedule)
            break
        case .edit:
            icMenu = UIImage(named: "ic_edit_mode")
            selector = #selector(editContact)
            break
        case .done:
            icMenu = UIImage(named: "ic_done")
            selector = #selector(doneEditing)
            break
        }
        
        currButtonType = type
        
        rightBarButton = UIBarButtonItem(image: icMenu, style: UIBarButtonItem.Style.plain, target: self, action: selector)
        navigationItem.rightBarButtonItem = rightBarButton
        //animateBlinking(type: currButtonType)
    }
    
    func animateBlinking(type: type){
        if(type == .flexifast){
            print("START_BLINKING")
            self.rightBarButton?.tintColor = UIColor.white.withAlphaComponent(0)
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.25) {
                
                UIView.setAnimationRepeatAutoreverses(true)
                
                UIView.animate(withDuration: 0.5, delay: 0, options: [.repeat, .autoreverse], animations: {
                    self.rightBarButton?.tintColor = UIColor.white.withAlphaComponent(1)
                }, completion: nil)
            }
        }
    }
    
    @objc func openFlexiFastPage(_ sender: AnyObject){
        isShouldCancelAPI = false
//        let storyboard = UIStoryboard(name: Constant.sharedIns.FLEXI_FAST_STORYBOARD, bundle: nil)
//        let navCon = storyboard.instantiateViewController(withIdentifier: Constant.sharedIns.FLEXI_FAST_OFFER_NAV) as! UINavigationController
//        
//        let vc = navCon.viewControllers.first as! FlexiFastOfferController
//        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func openMyDreamIntro(_ sender: AnyObject){
        //isShouldCancelAPI = false
    }
    
    @objc func openMyDreamProgress(_ sender: AnyObject){
        //isShouldCancelAPI = false
    }
    
    @objc func openContactUsPage(_ sender: AnyObject){
        
        //        var index = ["index" : 0]
        //        if(CustomUserDefaults.shared.isGuest()){
        //            index = ["index" : 1]
        //        }else{
        //            index = ["index" : 4]
        //        }
        //
        //        NotificationCenter.default.post(name: NSNotification.Name.NOTIF_SELECT_DRAWER_POS, object: nil, userInfo: index)
        
        //        isShouldCancelAPI = false
        //        switchViewController(segueIdentifier: Constant.sharedIns.SEGUE_CONTACT_US)
        
//        let storyboard = UIStoryboard(name: Constant.sharedIns.OTHERS_STORYBOARD, bundle: nil)
//        let navCon = storyboard.instantiateViewController(withIdentifier: Constant.sharedIns.CONTACT_ME) as! UINavigationController
//        let vc = navCon.viewControllers.first as! CallMeGeneralController
//        vc.frompage = "contactus"
//        present(navCon, animated: true, completion: nil)
    }
    
    @objc func closePage(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func logout(){
        let alert = UIAlertController(title: "Log Out",
                                      message: "Are you sure want to Log Out?",
                                      preferredStyle: .alert)
        
        let notCompleteAction = UIAlertAction(title: "No",
                                              style: .default) {
                                                [unowned self] action in
                                                self.notLogOut()
        }
        
        let yesCompleteAction = UIAlertAction(title: "Yes",
                                              style: .default) {
                                                [unowned self] action in
                                                self.yesLogOut()
        }
        
        alert.addAction(notCompleteAction)
        alert.addAction(yesCompleteAction)
        
        present(alert, animated: true)
    }
    
    func notLogOut() {
    }
    
    func yesLogOut() {
        AppController.sharedInstance.logout()
    }
    
    @objc func doneEditing(){
        if (CustomUserDefaults.shared.getTitleValue() == "Create Contact") {
            CommonUtils.shared.createContact(surname: "", lastName: "", birthday: "", homePhone: "", mobilePhone: "", homeEmail: "", workEmail: "")
        }
        navigationController?.popViewController(animated: true)
    }
    
    @objc func editContact(){
        navigationController?.popViewController(animated: true)
    }
    
    @objc func addContact(){
        let alert = UIAlertController(title: "Contact Data",
                                      message: "Create Contact",
                                      preferredStyle: .alert)
        
        let createNewAction = UIAlertAction(title: "New",
                                       style: .default) {
                                        [unowned self] action in
                                        self.addNewContact()
        }
        
        let openContactAction = UIAlertAction(title: "Import",
                                            style: .default) {
                                                [unowned self] action in
                                                self.openContact()
        }
        
        alert.addAction(createNewAction)
        alert.addAction(openContactAction)
        
        self.present(alert, animated: true) {
            alert.view.superview?.isUserInteractionEnabled = true
            alert.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.alertControllerBackgroundTapped)))
        }
    }
    
    func addActivityCase(){
        let alert = UIAlertController(title: "Activity",
                                      message: "Create Activity",
                                      preferredStyle: .alert)
        
        let createNewCase = UIAlertAction(title: "1st Activity",
                                            style: .default) {
                                                [unowned self] action in
                                                self.addNewCase()
        }
        
        let addActivity = UIAlertAction(title: "Next Activity",
                                              style: .default) {
                                                [unowned self] action in
                                                self.addActivityNew()
        }
        
        alert.addAction(createNewCase)
        alert.addAction(addActivity)
        
        self.present(alert, animated: true) {
            alert.view.superview?.isUserInteractionEnabled = true
            alert.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.alertControllerBackgroundTapped)))
        }
    }
    
    @objc func alertControllerBackgroundTapped()
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func addSchedule(){
//        let storyboard = UIStoryboard(name: Constant.sharedIns.DEAL_STORYBOARD, bundle: nil)
//        let navCon = storyboard.instantiateViewController(withIdentifier: Constant.sharedIns.CREATE_EDIT_CASE_NAV) as! UINavigationController
//        let vc = navCon.viewControllers.first as! CreateEditCaseViewController
//        vc.tabBarController?.tabBar.isHidden = true
//        vc.purpose = "Create"
//        navigationController?.pushViewController(vc, animated: true)
    }
    
    func addNewContact() {
//        let storyboard = UIStoryboard(name: Constant.sharedIns.CONTACT_STORYBOARD, bundle: nil)
//        let navCon = storyboard.instantiateViewController(withIdentifier: Constant.sharedIns.CREATE_EDIT_CONTACT_NAV) as! UINavigationController
//        let vc = navCon.viewControllers.first as! CreateEditContactViewController
//        vc.tabBarController?.tabBar.isHidden = true
//        vc.purpose = "Create"
//        vc.contactName = ""
//        vc.contactPhone = ""
//        navigationController?.pushViewController(vc, animated: true)
    }
    
    func addNewCase() {
//        let storyboard = UIStoryboard(name: Constant.sharedIns.DEAL_STORYBOARD, bundle: nil)
//        let navCon = storyboard.instantiateViewController(withIdentifier: Constant.sharedIns.CREATE_EDIT_CASE_NAV) as! UINavigationController
//        let vc = navCon.viewControllers.first as! CreateEditCaseViewController
//        vc.tabBarController?.tabBar.isHidden = true
//        vc.purpose = "Create"
//        navigationController?.pushViewController(vc, animated: true)
    }
    
    func addActivityNew() {
//        let storyboard = UIStoryboard(name: Constant.sharedIns.DEAL_STORYBOARD, bundle: nil)
//        let navCon = storyboard.instantiateViewController(withIdentifier: Constant.sharedIns.CREATE_EDIT_CASE_NAV) as! UINavigationController
//        let vc = navCon.viewControllers.first as! CreateEditCaseViewController
//        vc.tabBarController?.tabBar.isHidden = true
//        vc.purpose = "Edit"
//        vc.additionalPurpose = "addActivity"
//        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func profilePage () {
        print("go to profile page")
//        let storyboard = UIStoryboard(name: Constant.sharedIns.AGENT_STORYBOARD, bundle: nil)
//        let navCon = storyboard.instantiateViewController(withIdentifier: Constant.sharedIns.AGENT_PROFILE_NAV) as! UINavigationController
//        let vc = navCon.viewControllers.first as! AgentProfileViewController
//        vc.tabBarController?.tabBar.isHidden = true
//        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func openSettingsPage(_ sender: AnyObject){
        isShouldCancelAPI = false
        
//        let storyboard = UIStoryboard(name: Constant.sharedIns.MESSAGE_STORYBOARD, bundle: nil)
//        let navCon = storyboard.instantiateViewController(withIdentifier: Constant.sharedIns.MESSAGE_SETTINGS_NAV) as! UINavigationController
//        let vc = navCon.viewControllers.first as! MessageSettingsController
//        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func deleteMessage(_ sender: AnyObject){}
    
    @objc func openContractDetail(_ notif: Notification){
        isShouldCancelAPI = false
        
        let info = notif.userInfo! as NSDictionary
        let index = info["index"]
        let contractObj = info["contractObj"]
        print("CLICK_CONTRACT: \(String(describing: index))")
        
//        let storyboard = UIStoryboard(name: Constant.sharedIns.CONTRACT_STORYBOARD, bundle: nil)
//        let navCon = storyboard.instantiateViewController(withIdentifier: Constant.sharedIns.CONTRACT_NAV) as! UINavigationController
//        let vc = navCon.viewControllers.first as! ContractDetailController
//
//        ///USING INDEX -- CHOOSE ONE
//        if(index != nil){
//            vc.contractIndex = index as! Int
//        }
//
//        ///USING OBJECT DIRECTLY -- CHOOSE ONE
//        if (contractObj != nil){
//            vc.contractObj = contractObj as? ContractHistory
//        }
//
//        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func openHowToPay(_ notif: Notification){
        isShouldCancelAPI = false
        
        //        let storyboard = UIStoryboard(name: Constant.sharedIns.OTHERS_STORYBOARD, bundle: nil)
        //        let navCon = storyboard.instantiateViewController(withIdentifier: Constant.sharedIns.HOW_TO_PAY_NAV) as! UINavigationController
        //        let vc = navCon.viewControllers.first as! HowToPayNewController
        //        let contractNumber =  ""
        //        let amount = ""
        //        vc.contractnumber = contractNumber
        //        vc.contractamount = amount
        //        navigationController?.pushViewController(vc, animated: true )
        
//        let storyboard = UIStoryboard(name: "HowToPay", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "HowToPayID") as! HowToPayController
//
//        let info = notif.userInfo
//        if info != nil{
//            let data = (info! as Dictionary)["data"]
//            if data != nil{
//                vc.hashableData = data as! [AnyHashable : Any]
//            }
//        }
//
//        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func openPaymentLocator(_ sender: AnyObject){
        //        isShouldCancelAPI = false
        //
        //        let storyboard = UIStoryboard(name: Constant.sharedIns.OTHERS_STORYBOARD, bundle: nil)
        //        let navCon = storyboard.instantiateViewController(withIdentifier: Constant.sharedIns.POS_LOCATION_NAV) as! UINavigationController
        //        let vc = navCon.viewControllers.first as! StoreLocation
        //        vc.frompage = "Lokasi Pembayaran"
        //        navigationController?.pushViewController(vc, animated: true )
    }
    
    @objc func openInstallmentSchedule(_ notif: Notification){
        print("OPEN INSTALLMENT SCHEDULE")
        
        let info = notif.userInfo! as NSDictionary
        let contractNumber = info["contractNumber"] as! String
        
//        let storyboard = UIStoryboard(name: Constant.sharedIns.CONTRACT_STORYBOARD, bundle: nil)
//        let navCon = storyboard.instantiateViewController(withIdentifier: Constant.sharedIns.INST_SCHEDULE_NAV) as! UINavigationController
//        let vc = navCon.viewControllers.first as! InstallmentScheduleController
//
//        vc.contractNumber = contractNumber
//
//        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @objc func openCreditSim(_ notif: Notification){
        //        let storyboard = UIStoryboard(name: Constant.sharedIns.OTHERS_STORYBOARD, bundle: nil)
        //        let navCon = storyboard.instantiateViewController(withIdentifier: Constant.sharedIns.GENERAL_SIMULATION_NAV) as! UINavigationController
        //        let vc = navCon.viewControllers.first as! InstallmentSimulation
        
//        let storyboard = UIStoryboard(name: "CreditSimulation", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "GeneralSimulationID") as! GeneralSimulationController
//        navigationController?.pushViewController(vc, animated: true )
    }
    
    @objc func openPosLocator(_ notif: Notification){
        //        let storyboard = UIStoryboard(name: Constant.sharedIns.OTHERS_STORYBOARD, bundle: nil)
        //        let navCon = storyboard.instantiateViewController(withIdentifier: Constant.sharedIns.POS_LOCATION_NAV) as! UINavigationController
        //        let vc = navCon.viewControllers.first as! StoreLocation
        //        vc.frompage = "Lokasi Toko"
        //        navigationController?.pushViewController(vc, animated: true )
        
        let info = notif.userInfo! as NSDictionary
        let title = info["title"]
        let index = info["comIndex"]
        
//        let storyboard = UIStoryboard(name: "Maps", bundle: nil)
//        let navCon = storyboard.instantiateViewController(withIdentifier: "MapsLocationNavID") as! UINavigationController
//        let vc = navCon.viewControllers.first as! MapsLocatorController
//
//        if title == nil{
//            vc.titleLocation = "Lokasi Toko"
//        }else{
//            vc.titleLocation = title as! String
//        }
//
//        if index != nil{
//            vc.comTypeIndex = index as! Int
//        }
//        navigationController?.pushViewController(vc, animated: true )
    }
    
    @objc func openEShops(_ notif: Notification){
//        let storyboard = UIStoryboard(name: Constant.sharedIns.OTHERS_STORYBOARD, bundle: nil)
//        let navCon = storyboard.instantiateViewController(withIdentifier: Constant.sharedIns.E_SHOPS_NAV) as! UINavigationController
//        let vc = navCon.viewControllers.first as! eShopsController
//        navigationController?.pushViewController(vc, animated: true )
        
    }
    
    @objc func openOurGuarantee(_ notif: Notification){
        let storyboard = UIStoryboard(name: "OurGuarantee", bundle: nil)
        //let navCon = storyboard.instantiateViewController(withIdentifier: "OurGuaranteeNavID") as! UINavigationController
        //let vc = navCon.viewControllers.first as! OurGuaranteeController
        
        let vc = storyboard.instantiateViewController(withIdentifier: "OurGuaranteeID")
        navigationController?.pushViewController(vc, animated: true )
    }
    
    
    func tap(gesture: UITapGestureRecognizer) {
        CommonUtils.doneButtonAction()
    }
    
    func ApplicationState(){
        //let state = UIApplication.shared.applicationState
        /*if state == .inactive {
         print("app inactive")
         let storyboard = UIStoryboard(name: Constant.sharedIns.LOCKSCREEN_STORYBOARD, bundle: nil)
         let navCon = storyboard.instantiateViewController(withIdentifier: Constant.sharedIns.LOCK_SCREENNAV) as! UINavigationController
         let vc = navCon.viewControllers.first as! lockscreen
         present(navCon, animated: true, completion: nil)
         // inactive
         }else if state == .background {
         print("app in background")
         let storyboard = UIStoryboard(name: Constant.sharedIns.LOCKSCREEN_STORYBOARD, bundle: nil)
         let navCon = storyboard.instantiateViewController(withIdentifier: Constant.sharedIns.LOCK_SCREENNAV) as! UINavigationController
         let vc = navCon.viewControllers.first as! lockscreen
         present(navCon, animated: true, completion: nil)
         }else if state == .active {
         print("app active")
         // foreground
         let storyboard = UIStoryboard(name: Constant.sharedIns.LOCKSCREEN_STORYBOARD, bundle: nil)
         let navCon = storyboard.instantiateViewController(withIdentifier: Constant.sharedIns.LOCK_SCREENNAV) as! UINavigationController
         let vc = navCon.viewControllers.first as! lockscreen
         present(navCon, animated: true, completion: nil)
         }*/
//        let storyboard = UIStoryboard(name: Constant.sharedIns.LOCKSCREEN_STORYBOARD, bundle: nil)
//        let navCon = storyboard.instantiateViewController(withIdentifier: Constant.sharedIns.LOCK_SCREENNAV) as! UINavigationController
//        let vc = navCon.viewControllers.first as! lockscreen
//        present(vc, animated: true, completion: nil)
    }
    
    
    func makePhoneCall(message: String, phoneNum: String){
        let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
        
        let yesAction = UIAlertAction(title: "Ya", style: UIAlertAction.Style.default) {
            UIAlertAction in
            
            if let url = URL(string: "tel://\(phoneNum)"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
        
        let cancelAction = UIAlertAction(title: "Tidak", style: UIAlertAction.Style.default) {
            UIAlertAction in
        }
        alertController.addAction(yesAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func setActionForSosmed(btnFB: UIButton,
                            btnInsta: UIButton,
                            btnTwitter: UIButton,
                            btnLinkedIn: UIButton,
                            lblWebURL: UILabel){
        
        btnFB.addTarget(self, action: #selector(openFacebook(_:)), for: .touchUpInside)
        btnInsta.addTarget(self, action: #selector(openInstagram(_:)), for: .touchUpInside)
        btnTwitter.addTarget(self, action: #selector(openTwitter(_:)), for: .touchUpInside)
        btnLinkedIn.addTarget(self, action: #selector(openLinkedin(_:)), for: .touchUpInside)
        
        addTapEventForView(view: lblWebURL, action: #selector(openWebHomeCredit(_:)))
    }
    
    
    @objc func openFacebook(_ sender: UIButton) {
//        let fbUrl = NSURL(string: Constant.sharedIns.fbApp)
//        if UIApplication.shared.canOpenURL(fbUrl! as URL){
//            UIApplication.shared.openURL(fbUrl! as URL)
//            return
//        }
        
//        let storyboard = UIStoryboard(name: Constant.sharedIns.OTHERS_STORYBOARD, bundle: nil)
//        let navCon = storyboard.instantiateViewController(withIdentifier: Constant.sharedIns.WEBVIEW_ID) as! UINavigationController
//        let vc = navCon.viewControllers.first as! WebViewController
//        vc.url = Constant.sharedIns.URL_FB_HCID
//        present(navCon, animated: true, completion: nil)
    }
    
    
    @objc func openInstagram(_ sender: UIButton) {
//        let instagramUrl = NSURL(string: Constant.sharedIns.instagramApp)
//        if UIApplication.shared.canOpenURL(instagramUrl! as URL){
//            UIApplication.shared.openURL(instagramUrl! as URL)
//            return
//        }
        
//        let storyboard = UIStoryboard(name: Constant.sharedIns.OTHERS_STORYBOARD, bundle: nil)
//        let navCon = storyboard.instantiateViewController(withIdentifier: Constant.sharedIns.WEBVIEW_ID) as! UINavigationController
//        let vc = navCon.viewControllers.first as! WebViewController
//        vc.url = Constant.sharedIns.URL_INSTAGRAM_HCID
//        present(navCon, animated: true, completion: nil)
    }
    
    @objc func openTwitter(_ sender: UIButton) {
//        let twitterUrl = NSURL(string: Constant.sharedIns.twitterApp)
//        if UIApplication.shared.canOpenURL(twitterUrl!as URL){
//            UIApplication.shared.openURL(twitterUrl! as URL)
//            return
//        }
        
//        let storyboard = UIStoryboard(name: Constant.sharedIns.OTHERS_STORYBOARD, bundle: nil)
//        let navCon = storyboard.instantiateViewController(withIdentifier: Constant.sharedIns.WEBVIEW_ID) as! UINavigationController
//        let vc = navCon.viewControllers.first as! WebViewController
//        vc.url = Constant.sharedIns.URL_TWITTER_HCID
//        present(navCon, animated: true, completion: nil)
    }
    
    @objc func openLinkedin(_ sender: UIButton) {
//        let linkedinUrl = NSURL(string: Constant.sharedIns.linkedinApp)
//        if UIApplication.shared.canOpenURL(linkedinUrl!as URL){
//            UIApplication.shared.openURL(linkedinUrl! as URL)
//            return
//        }
        
//        let storyboard = UIStoryboard(name: Constant.sharedIns.OTHERS_STORYBOARD, bundle: nil)
//        let navCon = storyboard.instantiateViewController(withIdentifier: Constant.sharedIns.WEBVIEW_ID) as! UINavigationController
//        let vc = navCon.viewControllers.first as! WebViewController
//        vc.url = Constant.sharedIns.URL_LINKEDIN_HCID
//        present(navCon, animated: true, completion: nil)
    }
    
    @objc func openWebHomeCredit(_ sender: UIButton) {
//        let storyboard = UIStoryboard(name: Constant.sharedIns.OTHERS_STORYBOARD, bundle: nil)
//        let navCon = storyboard.instantiateViewController(withIdentifier: Constant.sharedIns.WEBVIEW_ID) as! UINavigationController
//        let vc = navCon.viewControllers.first as! WebViewController
//        vc.url = Constant.sharedIns.URL_WEBSITE_HCID
//        present(navCon, animated: true, completion: nil)
    }
    
}

