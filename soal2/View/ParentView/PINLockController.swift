import UIKit

class PINLockParentController: AlamofireBaseParentController{
    
    var isAppInBackgroundState : Bool = false
    static var isPinLockShown : Bool!
    var isShouldCancelAPI = false
    
    override func viewDidLoad() {
        isAppInBackgroundState = false
        super.viewDidLoad()
        PINLockParentController.isPinLockShown = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector(didEnterBackground(_:)), name: UIApplication.didEnterBackgroundNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(willEnterForeground(_:)), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: UIApplication.didEnterBackgroundNotification, object: nil)
        
        NotificationCenter.default.removeObserver(self, name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    
    @objc func didEnterBackground(_ sender: Notification){
        print("APP TO BACKGROUND")
        isShouldCancelAPI = false
        isAppInBackgroundState = true
        
        showPinLock(isAnimate: true)
    }
    
    @objc func willEnterForeground(_ sender: Notification){
        print("APP TO FOREGROUND")
        isAppInBackgroundState = false
    }
    
    func showPinLock(isAnimate: Bool){
        #if PRODUCTION
        if PINLockParentController.isPinLockShown == false{
            if(CustomUserDefaults.shared.isGuest() == false){
                PINLockParentController.isPinLockShown = true
                let storyboard = UIStoryboard(name: "LockScreen", bundle: nil)
                let navCon = storyboard.instantiateViewController(withIdentifier: "PinLockNavID") as! UINavigationController
                present(navCon, animated: isAnimate, completion: nil)
            }
        }
        #endif
    }
}

