//
//  ProfileViewController.swift
//  soal2
//
//  Created by Oscar Perdanakusuma Adipati on 28/12/20.
//

import Foundation
import UIKit
import CoreData
import Alamofire
import AlamofireObjectMapper
import Photos
import Kingfisher

class ProfileViewController: BaseParentController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var imgCalendar: UIImageView!
    @IBOutlet weak var rekamMedisView: UIView!
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var birthdayView: UIView!
    @IBOutlet weak var addressView: UIView!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var roundedView: UIView!
    @IBOutlet weak var txtRekamMedis: UITextField!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var lblBirthday: UILabel!
    @IBOutlet weak var txtAddress: UITextView!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var btnSave: UIButton!
    var resizeImage = UIImage()
    var imageData: Data!
    var birthdayPicker = UIDatePicker()
    var toolBar = UIToolbar()
    var insertUser: [NSManagedObject] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.navigationController == nil {
            return
        }
        setEnableTapGestureOnMainView(isEnable: false)
        
        setRoundedViewWithBorder(view: rekamMedisView, borderColor: Constant.sharedIns.color_grey_300, bgColor: Constant.sharedIns.color_white)
        setRoundedViewWithBorder(view: nameView, borderColor: Constant.sharedIns.color_grey_300, bgColor: Constant.sharedIns.color_white)
        setRoundedViewWithBorder(view: birthdayView, borderColor: Constant.sharedIns.color_grey_300, bgColor: Constant.sharedIns.color_white)
        setRoundedViewWithBorder(view: addressView, borderColor: Constant.sharedIns.color_grey_300, bgColor: Constant.sharedIns.color_white)
        setRoundedViewWithBorder(view: emailView, borderColor: Constant.sharedIns.color_grey_300, bgColor: Constant.sharedIns.color_white)
        
        setRoundedButton(button: btnSave, textColor: Constant.sharedIns.color_white, bgColor: Constant.sharedIns.color_green_800, isBold: true)
        imgProfile.setCircleBackgroundForView(view: imgProfile, bgColor: UIColor.clear)
        imgProfile.setCircleStrokeForView(view: imgProfile, strokeWidth: 0.5, strokeColor: UIColor.lightGray)
        addTapEventForView(view: imgProfile, action: #selector(imgProfileClicked))
        addTapEventForView(view: imgCalendar, action: #selector(calendarClicked))
        addTapEventForView(view: btnSave, action: #selector(saveClicked))
        checkPermission()
        callAPIProfile()
        let img = UIImage(named: "ic_profile")
        imageData = img!.jpegData(compressionQuality: 1)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        birthdayPicker.isHidden = true
        toolBar.isHidden = true
        roundedView.roundCorners(corners: [.topLeft, .topRight], radius: 20)
    }
            
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }
    
    @objc func imgProfileClicked () {
        optionsPickImage()
    }
    
    @objc func calendarClicked () {
        birthdayPicker.isHidden = false
        toolBar.isHidden = false
        birthdayPicker.datePickerMode = UIDatePicker.Mode.date
        birthdayPicker.locale = NSLocale(localeIdentifier: "id_ID") as Locale
        birthdayPicker.addTarget(self, action: #selector(birthdayDateChanged(_:)), for: UIControl.Event.valueChanged)
        let pickerSize : CGSize = birthdayPicker.sizeThatFits(CGSize.zero)
        birthdayPicker.frame = CGRect(x: self.view.frame.width / 14, y: self.view.frame.height / 4, width: self.view.frame.width * 0.85, height: pickerSize.height)
        birthdayPicker.setValue(UIColor.black, forKeyPath: "textColor")
        birthdayPicker.backgroundColor = UIColor.gray
        toolBar.sizeToFit()
        toolBar.backgroundColor = UIColor(string: Constant.sharedIns.color_blue)
        let doneButton = UIBarButtonItem(title: "Tutup", style: UIBarButtonItem.Style.plain, target: self, action: #selector(donePicker))
        toolBar.setItems([doneButton], animated: false)
        toolBar.frame = CGRect(x: self.view.frame.width / 14, y: self.view.frame.height / 4 + pickerSize.height, width: self.view.frame.width * 0.85, height: toolBar.frame.height)
        self.view.addSubview(birthdayPicker)
        self.view.addSubview(toolBar)
    }
    
    @objc func birthdayDateChanged(_ sender:UIDatePicker){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d MMMM yyyy"
        dateFormatter.dateStyle = DateFormatter.Style.long
        dateFormatter.timeStyle = DateFormatter.Style.none
        dateFormatter.locale = Locale(identifier: "id_ID")
        self.lblBirthday.text = dateFormatter.string(from: sender.date)
    }
    
    @objc func donePicker(){
        birthdayPicker.isHidden = true
        toolBar.isHidden = true
    }
    
    @objc func saveClicked () {
        if (txtName.text?.isReallyEmpty == true) {
            AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: Wordings.msg_no_agent_name, actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
        } else if (txtEmail.text?.isReallyEmpty == true) {
            AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: Wordings.msg_input_email, actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
        } else if (Constant.isValidEmail(testStr: txtEmail.text!) == false) {
            AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: Wordings.msg_wrong_email_format, actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
            return
        } else if (txtRekamMedis.text?.isReallyEmpty == true) {
            AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: Wordings.msg_input_rekam_medis, actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
        } else if (txtAddress.text?.isReallyEmpty == true) {
            AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: Wordings.msg_input_address, actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
        } else if (lblBirthday.text?.isReallyEmpty == true) {
            AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: Wordings.msg_input_birthday, actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
        } else {
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                return
            }
            let managedContext = appDelegate.persistentContainer.viewContext
            let insertAgent = NSEntityDescription.insertNewObject(forEntityName: "Profile", into: managedContext)
            insertAgent.setValue("1", forKey: "id")
            insertAgent.setValue("\(self.txtName.text!)", forKey: "nama")
            insertAgent.setValue("\(self.txtRekamMedis.text!)", forKey: "noRekamMedis")
            insertAgent.setValue("\(self.lblBirthday.text!)", forKey: "birthday")
            insertAgent.setValue("\(self.txtAddress.text!)", forKey: "address")
            insertAgent.setValue("\(self.txtEmail.text!)", forKey: "email")
            insertAgent.setValue(self.imageData!, forKey: "profileImage")
            do {
                try managedContext.save()
                self.insertUser.append(insertAgent)
                CustomUserDefaults.shared.setUsername("\(self.txtName.text!)")
                goToHomePage()
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
        }
    }
    
    @objc func optionsPickImage() {
        let alert = UIAlertController(title: "", message: "Choose Profile Photo", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "New Photo", style: .default, handler: { _ in
            self.openCamera()
        }))
        alert.addAction(UIAlertAction(title: "Take from Gallery", style: .default, handler: { _ in
            self.choosePictureFromLibrary()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    func choosePictureFromLibrary(){
        print("image click")
        let image = UIImagePickerController()
        image.delegate = self
        image.sourceType = .photoLibrary
        image.allowsEditing = false
        image.modalPresentationStyle = .fullScreen
        self.present(image, animated: true)
    }
    
    func openCamera() {
        print("camera click")
        let image = UIImagePickerController()
        image.delegate = self
        image.sourceType = .camera
        image.allowsEditing = false
        image.modalPresentationStyle = .fullScreen
        self.present(image, animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        var base64ImageData: String = ""
        if let image = info[.originalImage] as? UIImage {
            resizeImage = image.resizeUIImage(targetSize: CGSize(width: 350, height: 350))
            
            imageData = resizeImage.jpegData(compressionQuality: 100)!
            base64ImageData = imageData.base64EncodedString()
            imgProfile.contentMode = .scaleAspectFill
            imgProfile.image = resizeImage
        } else {
            print("error image")
        }
        print("processing save image")
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func saveImage(imageName: String){
        //create an instance of the FileManager
        let fileManager = FileManager.default
        //get the image path
        let imagePath = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(imageName)
        //get the image we took with camera
        let image = imgProfile.image!
        //get the PNG data for this image
        let data = image.pngData()
        //store it in the document directory
        fileManager.createFile(atPath: imagePath as String, contents: data, attributes: nil)
    }
    
    func getImage(imageName: String){
        let fileManager = FileManager.default
        let imagePath = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(imageName)
        if fileManager.fileExists(atPath: imagePath){
            imgProfile.image = UIImage(contentsOfFile: imagePath)
        }else{
            print("Panic! No Image!")
        }
    }
    
    func checkPermission() {
        let photoAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        switch photoAuthorizationStatus {
        case .authorized:
            print("Access is granted by user")
            self.addTapEventForView(view: self.imgProfile, action: #selector(self.optionsPickImage))
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization({
                (newStatus) in
                print("status is \(newStatus)")
                if newStatus ==  PHAuthorizationStatus.authorized {
                    self.addTapEventForView(view: self.imgProfile, action: #selector(self.optionsPickImage))
                    print("success")
                }
            })
            print("It is not determined until now")
        case .restricted:
            // same same
            print("User do not have access to photo album.")
        case .denied:
            // same same
            print("User has denied the permission.")
        case .limited: break
            // same same
        @unknown default: break
            // same same
        }
    }
    
    func callAPIProfile() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequestAgent = NSFetchRequest<NSManagedObject>(entityName: "Profile")
        let agentCodeKeyPredicate = NSPredicate(format: "id == %@", CustomUserDefaults.shared.getGuestID())
        fetchRequestAgent.predicate = agentCodeKeyPredicate
        do {
            let agents = try managedContext.fetch(fetchRequestAgent)
            if (agents.count == 0) {
                AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: Wordings.msg_no_agent_matched, actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
            } else {
                //CoreData
                let agent = agents[0]
                DispatchQueue.main.async {
                    self.txtName.text = agent.value(forKey: "nama") as? String
                    self.txtRekamMedis.text = agent.value(forKey: "noRekamMedis") as? String
                    self.txtEmail.text = agent.value(forKey: "email") as? String
                    self.txtAddress.text = agent.value(forKey: "address") as? String
                    self.lblBirthday.text = agent.value(forKey: "birthday") as? String
                    if let imageDatas = agent.value(forKey: "profileImage") as? NSData {
                        if let image = UIImage(data:imageDatas as Data) {
                            self.imgProfile.image = image
                            self.imageData = imageDatas as Data?
                        }
                    }
                }
            }
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    func goToHomePage() {
        let storyboard = UIStoryboard(name: Constant.sharedIns.HOMEPAGE_STORYBOARD, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: Constant.sharedIns.HOMEPAGE_NAV)
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
}
