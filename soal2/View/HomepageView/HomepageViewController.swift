//
//  HomepageViewController.swift
//  soal2
//
//  Created by Oscar Perdanakusuma Adipati on 28/12/20.
//

import Foundation
import UIKit

class HomepageViewController: BaseParentController, UNUserNotificationCenterDelegate {
    @IBOutlet weak var greetingsButton: UIBarButtonItem!
    @IBOutlet weak var inboxButton: UIBarButtonItem!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var informationView: UIView!
    var collectionViewArray = ["Konsultasi Dokter", "Poliklinik", "Medical Check Up", "Farmasi", "Laboratorium", "Promo Bulan Ini", "Dokter Kami", "Location", "Hubungi Kami (Gawat Darurat)"]
    let userNotificationCenter = UNUserNotificationCenter.current()
    var gradientLayer : CAGradientLayer!
    let colors = gradientColors()
    
    lazy var barButton: ButtonWithProperty = {
        let button = ButtonWithProperty()
        button.tintColor = .red
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.navigationController == nil {
            return
        }
        setEnableTapGestureOnMainView(isEnable: false)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UINib(nibName: "HospitalServiceCategory", bundle: nil), forCellWithReuseIdentifier: "HospitalServiceCategoryCell")
        guard let navigationBar = self.navigationController?.navigationBar else { return }

        navigationBar.addSubview(barButton)
        barButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            barButton.rightAnchor.constraint(equalTo: navigationBar.rightAnchor, constant: -20),
            barButton.bottomAnchor.constraint(equalTo: navigationBar.bottomAnchor, constant: -6)
        ])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        createGradientLayer()
        greetingsButton.title =  setGreetings() + ","
        lblName.text = "\(CustomUserDefaults.shared.getUsername())"
        DispatchQueue.main.async {
            self.tabBarController?.tabBar.isHidden = false
            self.informationView.roundCorners(corners: [.topLeft, .topRight], radius: 20)
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }
    func setGreetings()-> String{
        let hour = Calendar.current.component(.hour, from: Date())
        var hourTime: String?
        switch hour {
            case 6..<12 : hourTime = "\(NSLocalizedString("Selamat Pagi", comment: "Selamat Pagi"))"
            case 12 : hourTime = "\(NSLocalizedString("Selamat Siang", comment: "Selamat Siang"))"
            case 13..<17 : hourTime = "\(NSLocalizedString("Selamat Sore", comment: "Selamat Sore"))"
            case 17..<22 : hourTime = "\(NSLocalizedString("Selamat Malam", comment: "Selamat Malam"))"
            default: hourTime = "\(NSLocalizedString("Selamat Tidur", comment: "Selamat Tidur"))"
        }
        return hourTime!
    }
            
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func createGradientLayer() {
        view.backgroundColor = UIColor.clear
        let backgroundLayer = colors.gl
        backgroundLayer?.frame = view.frame
        view.layer.insertSublayer(backgroundLayer!, at: 0)
    }
    
    @IBAction func inboxClicked(_ sender: Any) {
        barButton.isRead = true
        let storyboard = UIStoryboard(name: Constant.sharedIns.NOTIFICATION_STORYBOARD, bundle: nil)
        let navCon = storyboard.instantiateViewController(withIdentifier: Constant.sharedIns.NOTIFICATION_NAV) as! UINavigationController
        let vc = navCon.viewControllers.first as! NotificationViewController
        vc.modalPresentationStyle = .fullScreen
        vc.tabBarController?.tabBar.isHidden = true
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension HomepageViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionViewArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat =  50
        let collectionViewSize = collectionView.frame.size.width - padding
        print("width:\(collectionView.frame.size.width)")
        return CGSize(width: collectionViewSize/4, height: collectionViewSize/2.8)
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : CategoryCellView = collectionView.dequeueReusableCell(withReuseIdentifier: "HospitalServiceCategoryCell", for: indexPath) as! CategoryCellView
        cell.lblCategory.text = self.collectionViewArray[indexPath.item]
        cell.lblCategory.textColor = .black
        //if get data from API and append it into array
//        let url = URL(string: productCategoryImage[indexPath.item])
//        cell.imgCategory?.kf.setImage(with: url)
//            cell.imgCategory.clipsToBounds = true
//            cell.imgCategory.setCircleBackgroundForView(view: cell.imgCategory, bgColor: UIColor.clear)
//            cell.imgCategory.setCircleStrokeForView(view: cell.imgCategory, strokeWidth: 0.5, strokeColor: UIColor.lightGray)
//            cell.imgCategory.layer.cornerRadius = cell.imgCategory.frame.height / 2
//        cell.imgCategory.contentMode = .scaleToFill
        return cell
    }

    /// Select collection view cell
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //if cell selected
    }
}

class gradientColors {
    var gl:CAGradientLayer!
    
    init() {
        let colorTop = UIColor(red: 255.0 / 255.0, green: 255.0 / 255.0, blue: 255.0 / 255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 51.0 / 255.0, green: 176.0 / 255.0, blue: 74.0 / 255.0, alpha: 1.0).cgColor
        
        self.gl = CAGradientLayer()
        self.gl.colors = [colorTop, colorBottom]
        self.gl.locations = [0.15, 0.85]
    }
}
