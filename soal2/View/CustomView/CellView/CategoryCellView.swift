//
//  CategoryCellView.swift
//  kemala
//
//  Created by Oscar Perdanakusuma Adipati on 04/04/20.
//  Copyright © 2020 Sarimin App. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

class CategoryCellView: UICollectionViewCell {
    
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var imgCategory: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}

class NotificationCellView: UITableViewCell {
    
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var imgDoctor: UIImageView!
    @IBOutlet weak var lblDoctorName: UILabel!
    @IBOutlet weak var lblDoctorSpecialist: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var imgChat: UIImageView!
    @IBOutlet weak var imgChatView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        imgChat.image = imgChat.image!.withRenderingMode(.alwaysTemplate)
        imgChat.tintColor = .green
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}

class ChatCellView: UITableViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgOutgoing: UIImageView!
    @IBOutlet weak var imgIncoming: UIImageView!
    @IBOutlet weak var chatView: UIView!
    @IBOutlet weak var lblChat: UILabel!
    @IBOutlet weak var outgoingView: UIView!
    @IBOutlet weak var incomingView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
