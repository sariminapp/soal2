//
//  PhoneViewController.swift
//  soal2
//
//  Created by Oscar Perdanakusuma Adipati on 27/12/20.
//

import UIKit
import MRCountryPicker

class PhoneViewController: InitialParentController, MRCountryPickerDelegate {
    static let path = Bundle.main.path(forResource: "Config", ofType: "plist")
    static let config = NSDictionary(contentsOfFile: path!)
    private static let baseURLString = config!["serverUrl"] as! String
    @IBOutlet weak var countryPicker: MRCountryPicker!
    @IBOutlet weak var countryFlag: UIImageView!
    @IBOutlet weak var phoneCode: UILabel!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var phoneView: UIView!
    @IBOutlet weak var btnNext: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        countryPicker.countryPickerDelegate = self
        countryPicker.showPhoneNumbers = true
        countryPicker.setCountry("ID")
        countryPicker.setLocale("id_ID")
        countryPicker.setCountryByName("Indonesia")
        countryPicker.isHidden = true
        
        customNavBar.isHidden = true
        setTextFieldColor(tField: txtPhone, color: Constant.sharedIns.color_black)
        setRoundedViewWithBorder(view: phoneView, borderColor: Constant.sharedIns.color_grey_300, bgColor: Constant.sharedIns.color_white)
        setRoundedButton(button: btnNext, textColor: Constant.sharedIns.color_white, bgColor: Constant.sharedIns.color_orange_800, isBold: true)
        addTapEventForView(view: countryFlag, action: #selector(flagClicked))
        addTapEventForView(view: btnNext, action: #selector(nextClicked))
                
        txtPhone.delegate = self
        setEnableTapGestureOnMainView(isEnable: false)
    }

    func countryPhoneCodePicker(_ picker: MRCountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        self.phoneCode.text = phoneCode
        self.countryFlag.image = flag
        picker.isHidden = true
    }
    
    @objc func flagClicked () {
        countryPicker.isHidden = false
    }
    
    @objc func nextClicked () {
        if(isConnectingInternet()==false){
            AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: Wordings.msg_internet_error, actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
            return
        }else if(txtPhone.text?.isReallyEmpty == true){
            AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: Wordings.msg_input_phone, actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
            return
        }else if (txtPhone.text![txtPhone.text!.startIndex] != "8") {
            AlertViewUtil.sharedIns.showAlertFeedback(self, messageText: Wordings.msg_phone_starts_without_local_area, actionButton1: Wordings.BTN_CLOSE, actionButton2: "")
            return
        }else{
            VerifyAPI.sendVerificationCode(phoneCode.text!, txtPhone.text!)
            codeVerificationViewController()
        }
    }
    
    func codeVerificationViewController() {
        let storyboard = UIStoryboard(name: Constant.sharedIns.MAIN_STORYBOARD, bundle: nil)
        let navCon = storyboard.instantiateViewController(withIdentifier: Constant.sharedIns.CODEVER_NAV) as! UINavigationController
        let vc = navCon.viewControllers.first as! CodeVerificationViewController
        vc.countryCode = phoneCode.text!
        vc.phoneNumber = txtPhone.text!
        vc.modalPresentationStyle = .fullScreen
        navigationController?.pushViewController(vc, animated: true)
    }
}

