//
//  Profile+CoreDataProperties.swift
//  
//
//  Created by Oscar Perdanakusuma Adipati on 28/12/20.
//
//

import Foundation
import CoreData


extension Profile {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Profile> {
        return NSFetchRequest<Profile>(entityName: "Profile")
    }

    @NSManaged public var id: String?
    @NSManaged public var profileImage: Data?
    @NSManaged public var noRekamMedis: String?
    @NSManaged public var nama: String?
    @NSManaged public var birthday: String?
    @NSManaged public var address: String?
    @NSManaged public var email: String?

}
