//
//  CustomUserDefaults.swift
//  homecredit
//
//  Created by Oscar Perdanakusuma - Private on 9/11/17.
//  Copyright © 2017 Home Credit Indonesia. All rights reserved.
//

import Foundation

class CustomUserDefaults{
    
    static let shared = CustomUserDefaults()
    
    let KEY_PHONE_NUMBER = "phone_number"
    let KEY_PIN = "hashed"
    let KEY_UNLOCK_ATTEMPT = "attempt"
    let KEY_SERVER_DATE = "server_date"
    let KEY_ACCESS_TOKEN = "access_token"
    let KEY_REFRESH_TOKEN = "refresh_token"
    let KEY_TOKEN_EXPIRE_TIME = "token_expire_time"
    let KEY_HEADER_THEME_COLOR = "header_theme_color"
    let KEY_HEADER_CONTACT_CASE = "header_contact_case"
    let KEY_HEADER_CASE = "header_case"
    let KEY_HEADER_OPEN_MEETING = "header_open_meeting"
    let KEY_HEADER_OPEN_CASE = "header_open_case"
    let KEY_CONTACT = "contact_picker"
    let KEY_THEME_COLOR = "theme_color"
    let KEY_APP_COLOR = "app_color"
    let KEY_USERNAME = "username"
    let KEY_PASSWORD = "password"
    let KEY_FCM_TOKEN = "FCM_token"
    let KEY_DUMMY_ACCOUNT = "dummy_account"
    let KEY_GUEST_ID = "guest_id"
    let KEY_GUEST_TOKEN = "guest_token"
    let KEY_GUEST_REFRESH_TOKEN = "guest_refresh_token"
    let KEY_AS_GUEST = "guest"
    let KEY_LATITUDE = "latitude"
    let KEY_LONGITUDE = "longitude"
    let KEY_EMAIL = "email"
    let KEY_SUBCELL = "subcell"
    let KEY_SUBCELLROW = "subcellrow"
    let KEY_UNREAD_MSG = "unread"
    let KEY_ACTIVITY_POPUP = "activity_pop_up"
    let KEY_PURPOSE_VALUE = "purpose_value"
    let KEY_CASE_TABLE = "case_table"
    let KEY_FIRST_ACTIVITY_STATUS = "first_activity_status"
    let KEY_DIRECTION_MODE = "direction_mode"
    let KEY_PLACEINFORMATION_MODE = "place_information_mode"
    let KEY_NEARBYPLACE_MODE = "nearby_place_mode"
    let KEY_MAP_STYLE = "map_style"
    let KEY_MAPZOOM_LEVEL = "map_zoom_level"
    let KEY_TITLE_VALUE = "title_value"
    let KEY_FAMILY_VIEW = "contact_family_view"
    let KEY_ADDITIONALINFO_VIEW = "contact_additionalinfo_view"
    let KEY_ACTIVITY_DATE = "activity_date"
    let KEY_CASE_NOTES_EMOTICON = "case_notes_emoticon"
    let KEY_CHILDREN_INDEX = "children_index"
    let KEY_BROTHER_INDEX = "brother_index"
    let KEY_SISTER_INDEX = "sister_index"
    let KEY_MONTH_INDEX = "month_index"
    let KEY_YEAR_INDEX = "year_index"
    let KEY_CALENDAR_EVENTID = "calendar_eventid"
    let KEY_USER_ID = "user_id"
    let KEY_CURRENT_LOCATION = "current_location"
    let KEY_FILTER_PLACE = "filter_place"
    let KEY_PRODUCTS_IDS = "product_ids"
    let KEY_CATEGORY_IDS = "category_ids"
    let KEY_PRODUCTS_NAMES = "product_names"
    let KEY_PRODUCTS_SIZES = "product_sizes"
    let KEY_PRODUCTS_PRICES = "product_prices"
    let KEY_PRODUCTS_QUANTITYS = "product_quantitys"
    let KEY_CARDHOLDER_NAME = "cardholder_name"
    let KEY_CARDHOLDER_NUMBER = "cardholder_number"
    let KEY_CARDHOLDER_CVC = "cardholder_cvc"
    let KEY_CARDHOLDER_MONTH = "cardholder_month"
    let KEY_CARDHOLDER_YEAR = "cardholder_year"
    
    let GREEN_THEME = 0
    let ORANGE_THEME = 1
    let RED_THEME = 2
    
    /** PHONE NUMBER **/
    func setPhoneNumber(_ data: String){
        UserDefaults.standard.set(data, forKey: KEY_PHONE_NUMBER)
    }
    
    func getPhoneNumber()->String{
        guard let data = UserDefaults.standard.string(forKey: KEY_PHONE_NUMBER) else {
            return ""
        }
        return data
    }
    
    /** HASHED PIN **/
    func setPIN(_ data: String){
        UserDefaults.standard.set(data, forKey: KEY_PIN)
    }
    
    func getPIN()->String{
        guard let data = UserDefaults.standard.string(forKey: KEY_PIN) else {
            return ""
        }
        return data
    }
    
    /** UNLOCK ATTEMPT **/
    func setAttempt(_ data: Int){
        UserDefaults.standard.set(data, forKey: KEY_UNLOCK_ATTEMPT)
    }
    
    func getAttempt()-> Int{
        guard UserDefaults.standard.integer(forKey: KEY_UNLOCK_ATTEMPT) >= 0 else {
            return 0
        }
        
        return UserDefaults.standard.integer(forKey: KEY_UNLOCK_ATTEMPT)
    }
    
    /** CONTACT FAMILY VIEW **/
    func setContactFamilyView(_ data: Int){
        UserDefaults.standard.set(data, forKey: KEY_FAMILY_VIEW)
    }
    
    func getContactFamilyView()-> Int{
        return UserDefaults.standard.integer(forKey: KEY_FAMILY_VIEW)
    }
    
    /** CONTACT ADDITIONAL INFO VIEW **/
    func setContactAdditionalInfoView(_ data: Int){
        UserDefaults.standard.set(data, forKey: KEY_ADDITIONALINFO_VIEW)
    }
    
    func getContactAdditionalInfoView()->Int{
        return UserDefaults.standard.integer(forKey: KEY_ADDITIONALINFO_VIEW)
    }
    
    /** CONTACT CHILDREN INDEX **/
    func setContactChildrenIndex(_ data: Int){
        UserDefaults.standard.set(data, forKey: KEY_CHILDREN_INDEX)
    }
    
    func getContactChildrenIndex()-> Int{
        return UserDefaults.standard.integer(forKey: KEY_CHILDREN_INDEX)
    }
    
    /** CONTACT USER ID **/
    func setContactID(_ data: String){
        UserDefaults.standard.set(data, forKey: KEY_USER_ID)
    }
    
    func getContactID()->String{
        guard let data = UserDefaults.standard.string(forKey: KEY_USER_ID) else {
            return ""
        }
        return data
    }
    
    /** CONTACT BROTHER INDEX **/
    func setContactBrotherIndex(_ data: Int){
        UserDefaults.standard.set(data, forKey: KEY_BROTHER_INDEX)
    }
    
    func getContactBrotherIndex()-> Int{
        return UserDefaults.standard.integer(forKey: KEY_BROTHER_INDEX)
    }
    
    /** CONTACT SISTER INDEX **/
    func setContactSisterIndex(_ data: Int){
        UserDefaults.standard.set(data, forKey: KEY_SISTER_INDEX)
    }
    
    func getContactSisterIndex()-> Int{
        return UserDefaults.standard.integer(forKey: KEY_SISTER_INDEX)
    }

    /** UNREAD INBOX COUNT **/
    func setUnreadCount(_ data: Int){
        UserDefaults.standard.set(data, forKey: KEY_UNREAD_MSG)
    }
    
    func getUnreadCount()-> Int{
        guard UserDefaults.standard.integer(forKey: KEY_UNREAD_MSG) >= 0 else {
            return 0
        }
        
        return UserDefaults.standard.integer(forKey: KEY_UNREAD_MSG)
    }
    
    /** SERVER DATE **/
    func setServerDate(_ data: String){
        UserDefaults.standard.set(data, forKey: KEY_SERVER_DATE)
    }
    
    func getServerDate()->String{
        guard let data = UserDefaults.standard.string(forKey: KEY_SERVER_DATE) else {
            return ""
        }
        return data
    }
    
    /** productIDS **/
    func setproductIDS(_ data: [String]){
        UserDefaults.standard.set(data, forKey: KEY_PRODUCTS_IDS)
    }
    
    func getproductIDS()->[String]{
        let data = UserDefaults.standard.stringArray(forKey: KEY_PRODUCTS_IDS) ?? [String]()
        return data
    }
    
    /** categoryIDS **/
    func setcategoryIDS(_ data: [String]){
        UserDefaults.standard.set(data, forKey: KEY_CATEGORY_IDS)
    }
    
    func getcategoryIDS()->[String]{
        let data = UserDefaults.standard.stringArray(forKey: KEY_CATEGORY_IDS) ?? [String]()
        return data
    }
    
    /** productName **/
    func setproductNameS(_ data: [String]){
        UserDefaults.standard.set(data, forKey: KEY_PRODUCTS_NAMES)
    }
    
    func getproductNameS()->[String]{
        let data = UserDefaults.standard.stringArray(forKey: KEY_PRODUCTS_NAMES) ?? [String]()
        return data
    }
    
    /** productSize **/
    func setproductSizeS(_ data: [String]){
        UserDefaults.standard.set(data, forKey: KEY_PRODUCTS_SIZES)
    }
    
    func getproductSizeS()->[String]{
        let data = UserDefaults.standard.stringArray(forKey: KEY_PRODUCTS_SIZES) ?? [String]()
        return data
    }
    
    /** productPrice **/
    func setproductPriceS(_ data: [String]){
        UserDefaults.standard.set(data, forKey: KEY_PRODUCTS_PRICES)
    }
    
    func getproductPriceS()->[String]{
        let data = UserDefaults.standard.stringArray(forKey: KEY_PRODUCTS_PRICES) ?? [String]()
        return data
    }
    
    /** productQuantity **/
    func setproductQuantityS(_ data: [String]){
        UserDefaults.standard.set(data, forKey: KEY_PRODUCTS_QUANTITYS)
    }
    
    func getproductQuantityS()->[String]{
        let data = UserDefaults.standard.stringArray(forKey: KEY_PRODUCTS_QUANTITYS) ?? [String]()
        return data
    }
    
    /** CURRENT LOCATION **/
    func setCurrentLocation(_ data: String){
        UserDefaults.standard.set(data, forKey: KEY_CURRENT_LOCATION)
    }
    
    func getCurrentLocation()->String{
        guard let data = UserDefaults.standard.string(forKey: KEY_CURRENT_LOCATION) else {
            return ""
        }
        return data
    }
    
    /** FILTER PLACE **/
    func setFilterPlace(_ data: String){
        UserDefaults.standard.set(data, forKey: KEY_FILTER_PLACE)
    }
    
    func getFilterPlace()->String{
        guard let data = UserDefaults.standard.string(forKey: KEY_FILTER_PLACE) else {
            return ""
        }
        return data
    }
    
    
    /** ACCESS TOKEN **/
    func setAccessToken(_ data: String){
        UserDefaults.standard.set(data, forKey: KEY_ACCESS_TOKEN)
    }
    
    func getAccessToken()->String{
        guard let data = UserDefaults.standard.string(forKey: KEY_ACCESS_TOKEN) else {
            return ""
        }
        return data
    }
    
    /** REFRESH TOKEN **/
    func setRefreshToken(_ data: String){
        UserDefaults.standard.set(data, forKey: KEY_REFRESH_TOKEN)
    }
    
    func getRefreshToken()->String{
        guard let data = UserDefaults.standard.string(forKey: KEY_REFRESH_TOKEN) else {
            return ""
        }
        return data
    }
    
    /** TOKEN EXPIRED TIME**/
    func setTokenExpireTime(_ data: String){
        UserDefaults.standard.set(data, forKey: KEY_TOKEN_EXPIRE_TIME)
    }
    
    func getTokenExpireTime()->String{
        guard let data = UserDefaults.standard.string(forKey: KEY_TOKEN_EXPIRE_TIME) else {
            return ""
        }
        return data
    }
    
    /** CONTACT CASE HEADER STRING **/
    func setHeaderContactCase(_ data: Int){
        UserDefaults.standard.set(data, forKey: KEY_HEADER_CONTACT_CASE)
    }
    
    func getHeaderContactCase()->Int{
        return UserDefaults.standard.integer(forKey: KEY_HEADER_CONTACT_CASE)
    }
    
    /** CASE HEADER STRING **/
    func setHeaderCase(_ data: Int){
        UserDefaults.standard.set(data, forKey: KEY_HEADER_CASE)
    }
    
    func getHeaderCase()->Int{
        return UserDefaults.standard.integer(forKey: KEY_HEADER_CASE)
    }
    
    /** OPEN MEETING HEADER STRING **/
    func setHeaderOpenMeeting(_ data: Int){
        UserDefaults.standard.set(data, forKey: KEY_HEADER_OPEN_MEETING)
    }
    
    func getHeaderOpenMeeting()->Int{
        return UserDefaults.standard.integer(forKey: KEY_HEADER_OPEN_MEETING)
    }
    
    /** OPEN CASE HEADER STRING **/
    func setHeaderOpenCase(_ data: Int){
        UserDefaults.standard.set(data, forKey: KEY_HEADER_OPEN_CASE)
    }
    
    func getHeaderOpenCase()->Int{
        return UserDefaults.standard.integer(forKey: KEY_HEADER_OPEN_CASE)
    }
    
    /** ACTIVITY DATE **/
    func setActivityDate(_ data: String){
        UserDefaults.standard.set(data, forKey: KEY_ACTIVITY_DATE)
    }
    
    func getActivityDate()->String{
        guard let data = UserDefaults.standard.string(forKey: KEY_ACTIVITY_DATE) else {
            return ""
        }
        return data
    }
    
    
    /** HEADER THEME COLOR **/
    func setHeaderColor(_ data: Int){
        UserDefaults.standard.set(data, forKey: KEY_HEADER_THEME_COLOR)
    }
    
    func getHeaderColor()->Int{
        return UserDefaults.standard.integer(forKey: KEY_HEADER_THEME_COLOR)
    }
    
    
    /** MAIN THEME COLOR **/
    func setThemeColor(_ data: Int){
        UserDefaults.standard.set(data, forKey: KEY_THEME_COLOR)
    }
    
    func getThemeColor()->Int{
        return UserDefaults.standard.integer(forKey: KEY_THEME_COLOR)
    }
    
    /** MONTH CALENDAR **/
    func setMonthCalendar(_ data: Int){
        UserDefaults.standard.set(data, forKey: KEY_MONTH_INDEX)
    }
    
    func getMonthCalendar()->Int{
        return UserDefaults.standard.integer(forKey: KEY_MONTH_INDEX)
    }
    
    /** YEAR CALENDAR **/
    func setYearCalendar(_ data: Int){
        UserDefaults.standard.set(data, forKey: KEY_YEAR_INDEX)
    }
    
    func getYearCalendar()->Int{
        return UserDefaults.standard.integer(forKey: KEY_YEAR_INDEX)
    }
    
    /** USERNAME **/
    func setUsername(_ data: String){
        UserDefaults.standard.set(data, forKey: KEY_USERNAME)
    }
    
    func getUsername()->String{
        guard let data = UserDefaults.standard.string(forKey: KEY_USERNAME) else {
            return ""
        }
        return data
    }
    
    /** PASSWORD **/
    func setPassword(_ data: String){
        UserDefaults.standard.set(data, forKey: KEY_PASSWORD)
    }
    
    func getPassword()->String{
        guard let data = UserDefaults.standard.string(forKey: KEY_PASSWORD) else {
            return ""
        }
        return data
    }
    
    /** FCM TOKEN **/
    func setFCMToken(_ data: String){
        UserDefaults.standard.set(data, forKey: KEY_FCM_TOKEN)
    }
    
    func getFCMToken()->String{
        guard let data = UserDefaults.standard.string(forKey: KEY_FCM_TOKEN) else {
            return ""
        }
        return data
    }
    
    
    /** DUMMY ACCOUNT **/
    func setDummyAccount(_ data: Bool){
        UserDefaults.standard.set(data, forKey: KEY_DUMMY_ACCOUNT)
    }
    
    func isDummyAccount()->Bool{
        return UserDefaults.standard.bool(forKey: KEY_DUMMY_ACCOUNT)
    }
    
    /** PURPOSE VALUE **/
    func setPurposeValue(_ data: String){
        UserDefaults.standard.set(data, forKey: KEY_PURPOSE_VALUE)
    }
    
    func getPurposeValue()->String{
        guard let data = UserDefaults.standard.string(forKey: KEY_PURPOSE_VALUE) else {
            return ""
        }
        return data
    }
    
    /** CASE TABLE **/
    func setCaseTable(_ data: String){
        UserDefaults.standard.set(data, forKey: KEY_CASE_TABLE)
    }
    
    func getCaseTable()->String{
        guard let data = UserDefaults.standard.string(forKey: KEY_CASE_TABLE) else {
            return ""
        }
        return data
    }
    
    /** FIRST ACTIVITY STATUS **/
    func setFirstActivityStatus(_ data: String){
        UserDefaults.standard.set(data, forKey: KEY_FIRST_ACTIVITY_STATUS)
    }
    
    func getFirstActivityStatus()->String{
        guard let data = UserDefaults.standard.string(forKey: KEY_FIRST_ACTIVITY_STATUS) else {
            return ""
        }
        return data
    }
    
    /** TITLE VALUE **/
    func setTitleValue(_ data: String){
        UserDefaults.standard.set(data, forKey: KEY_TITLE_VALUE)
    }
    
    func getTitleValue()->String{
        guard let data = UserDefaults.standard.string(forKey: KEY_TITLE_VALUE) else {
            return ""
        }
        return data
    }
    
    /** DIRECTION MODE **/
    func setDirectionMode(_ data: Int){
        UserDefaults.standard.set(data, forKey: KEY_DIRECTION_MODE)
    }
    
    func getDirectionMode()->Int{
        return UserDefaults.standard.integer(forKey: KEY_DIRECTION_MODE)
    }
    
    /** PLACE INFORMATION MODE **/
    func setPlaceInformationMode(_ data: Int){
        UserDefaults.standard.set(data, forKey: KEY_PLACEINFORMATION_MODE)
    }
    
    func getPlaceInformationMode()->Int{
        return UserDefaults.standard.integer(forKey: KEY_PLACEINFORMATION_MODE)
    }
    
    /** NEARBY PLACE MODE **/
    func setNearbyPlaceMode(_ data: Int){
        UserDefaults.standard.set(data, forKey: KEY_NEARBYPLACE_MODE)
    }
    
    func getNearbyPlaceMode()->Int{
        return UserDefaults.standard.integer(forKey: KEY_NEARBYPLACE_MODE)
    }
    
    /** MAP STYLE **/
    func setMapStyle(_ data: Int){
        UserDefaults.standard.set(data, forKey: KEY_MAP_STYLE)
    }
    
    func getMapStyle()->Int{
        return UserDefaults.standard.integer(forKey: KEY_MAP_STYLE)
    }
    
    /** MAP ZOOM LEVEL **/
    func setMapZoomLevel(_ data: Int){
        UserDefaults.standard.set(data, forKey: KEY_MAPZOOM_LEVEL)
    }
    
    func getMapZoomLevel()-> Int{
        return UserDefaults.standard.integer(forKey: KEY_MAPZOOM_LEVEL)
    }
    
    /** ACTIVITY POP UP **/
    func setActivityComplete(_ data: Bool){
        UserDefaults.standard.set(data, forKey: KEY_ACTIVITY_POPUP)
    }
    
    func isActivityComplete()->Bool{
        return UserDefaults.standard.bool(forKey: KEY_ACTIVITY_POPUP)
    }
    
    /** CASE NOTES EMOTICON **/
    func setCaseNotesEmoticon(_ data: String){
        UserDefaults.standard.set(data, forKey: KEY_CASE_NOTES_EMOTICON)
    }
    
    func getCaseNotesEmoticon()->String{
        guard let data = UserDefaults.standard.string(forKey: KEY_CASE_NOTES_EMOTICON) else {
            return ""
        }
        return data
    }
    
    /** CALENDAR EVENT ID **/
    func setCalendarEventId(_ data: String){
        UserDefaults.standard.set(data, forKey: KEY_CALENDAR_EVENTID)
    }
    
    func getCalendarEventId()->String{
        guard let data = UserDefaults.standard.string(forKey: KEY_CALENDAR_EVENTID) else {
            return ""
        }
        return data
    }
    
    /** GUEST ID / UUID **/
    func setGuestID(_ data: String){
        UserDefaults.standard.set(data, forKey: KEY_GUEST_ID)
    }
    
    func getGuestID()->String{
        guard let data = UserDefaults.standard.string(forKey: KEY_GUEST_ID) else {
            return ""
        }
        return data
    }
    
    /** GUEST TOKEN **/
    func setGuestAccessToken(_ data: String){
        UserDefaults.standard.set(data, forKey: KEY_GUEST_TOKEN)
    }
    
    func getGuestAccessToken()->String{
        guard let data = UserDefaults.standard.string(forKey: KEY_GUEST_TOKEN) else {
            return ""
        }
        return data
    }
    
    
    /** GUEST REFRESH TOKEN **/
    func setGuestRefreshToken(_ data: String){
        UserDefaults.standard.set(data, forKey: KEY_GUEST_REFRESH_TOKEN)
    }
    
    func getGuestRefreshToken()->String{
        guard let data = UserDefaults.standard.string(forKey: KEY_GUEST_REFRESH_TOKEN) else {
            return ""
        }
        return data
    }
    
    /** GET CONTACT **/
    func setContact(_ data: String){
        UserDefaults.standard.set(data, forKey: KEY_CONTACT)
    }
    
    func getContact()->String{
        guard let data = UserDefaults.standard.string(forKey: KEY_CONTACT) else {
            return ""
        }
        return data
    }
    
    /** AS GUEST **/
    func setAsGuest(_ data: Bool){
        UserDefaults.standard.set(data, forKey: KEY_AS_GUEST)
    }
    
    func isGuest()->Bool{
        return UserDefaults.standard.bool(forKey: KEY_AS_GUEST)
    }
    
    /** LATITUDE **/
    func setLatitude(_ data: String){
        UserDefaults.standard.set(data, forKey: KEY_LATITUDE)
    }
    
    func getCurrentLatitude()->String{
        guard let data = UserDefaults.standard.string(forKey: KEY_LATITUDE) else {
            return "0"
        }

        return data
    }
    
    /** LONGITUDE **/
    func setLongitude(_ data: String){
        UserDefaults.standard.set(data, forKey: KEY_LONGITUDE)
    }
    
    func getCurrentLongitude()->String{
        guard let data = UserDefaults.standard.string(forKey: KEY_LONGITUDE) else {
            return "0"
        }
        
        return data
    }
    
    func setEmail(_ data: String){
         UserDefaults.standard.set(data, forKey: KEY_EMAIL)
    }
    
    func getEmail()->String{
        guard let data = UserDefaults.standard.string(forKey: KEY_EMAIL) else {
            return ""
        }
        
        return data
    }
    
    /** CARD HOLDER NAME **/
    func setCardholderName(_ data: String){
        UserDefaults.standard.set(data, forKey: KEY_CARDHOLDER_NAME)
    }
    
    func getCardholderName()->String{
        guard let data = UserDefaults.standard.string(forKey: KEY_CARDHOLDER_NAME) else {
            return ""
        }
        return data
    }
    
    /** CARD HOLDER NUMBER **/
    func setCardholderNumber(_ data: String){
        UserDefaults.standard.set(data, forKey: KEY_CARDHOLDER_NUMBER)
    }
    
    func getCardholderNumber()->String{
        guard let data = UserDefaults.standard.string(forKey: KEY_CARDHOLDER_NUMBER) else {
            return ""
        }
        return data
    }
    
    /** CVC **/
    func setCardholderCVC(_ data: String){
        UserDefaults.standard.set(data, forKey: KEY_CARDHOLDER_CVC)
    }
    
    func getCardholderCVC()->String{
        guard let data = UserDefaults.standard.string(forKey: KEY_CARDHOLDER_CVC) else {
            return ""
        }
        return data
    }
    
    /** CARD HOLDER MONTH **/
    func setCardholderMonth(_ data: Int){
        UserDefaults.standard.set(data, forKey: KEY_CARDHOLDER_MONTH)
    }
    
    func getCardholderMonth()-> Int{
        return UserDefaults.standard.integer(forKey: KEY_CARDHOLDER_MONTH)
    }
    
    /** CARD HOLDER YEAR **/
    func setCardholderYear(_ data: Int){
        UserDefaults.standard.set(data, forKey: KEY_CARDHOLDER_YEAR)
    }
    
    func getCardholderYear()-> Int{
        return UserDefaults.standard.integer(forKey: KEY_CARDHOLDER_YEAR)
    }
    
    func saveUserCredential(phoneNumber: String, PIN: String){
        setThemeColor(0)
        setPhoneNumber(phoneNumber)
        
        saveHashedPIN(PIN: PIN)
    }
    
    func saveHashedPIN(PIN: String){
//        let hashedPIN = PinHelper.shared.genHashedPIN(plainPIN: PIN)
//        print("HASHED_RANDOM: \(hashedPIN)")
//        setPIN(hashedPIN)
    }
    
    func deleteUserData(){
        setAccessToken("")
        setRefreshToken("")
        setUsername("")
        setPhoneNumber("")
        setPassword("")
        setGuestID("")
    }
    func setSubCell(_ data: String){
        UserDefaults.standard.set(data, forKey: KEY_SUBCELL)
    }
    
    func getSubCell()->String{
        guard let data = UserDefaults.standard.string(forKey: KEY_SUBCELL) else {
            return ""
        }
        
        return data
    }
    func setSubCellRow(_ data: String){
        UserDefaults.standard.set(data, forKey: KEY_SUBCELLROW)
    }
    
    func getSubCellRow()->String{
        guard let data = UserDefaults.standard.string(forKey: KEY_SUBCELLROW) else {
            return ""
        }
        
        return data
    }
}
